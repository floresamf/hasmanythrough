class Appointment < ActiveRecord::Base
  belongs_to :specialist
  belongs_to :patient

  validates_numericality_of :fee, greater_than: 0, message: 'Fees must be a number greater than 0'
  validates_presence_of :fee, :specialist_id, :patient_id, message: 'You must enter a Fee amount'
end
