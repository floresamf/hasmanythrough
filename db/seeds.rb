# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

specialists = Specialist.create   ([
{ name: 'Minnie Mouse', specialty: 'Brains'},
    { name: 'Donald Duck', specialty: 'Guts'},
    { name: 'Goofy', specialty: 'Emotions'},
])
patients = Patient.create  ([
    { name: 'Daffy Duck', street_address: '2111 Avenue X'},
    { name: 'Buggs Bunny', street_address: '666 Elm Street'},
    { name: 'Wile E. Coyote', street_address: '34456 Tobacco Road'},
    { name: 'Porky Pig', street_address: '4567 Main'},
    { name: 'Elmer Fudd', street_address: '1187 11th Ave'},
])
insurance = Insurance.create  ([
    { name: 'Broke Neck Insurance', street_address: '100 A Street'},
    { name: 'Blue Cross', street_address: '200 B Street'},
    { name: 'Protect yer Guts', street_address: '300 C Street'},
    { name: 'Money Sink Insurance', street_address: '400 D Street'},
    { name: 'Galaxy Insurance', street_address: '500 E Street'},
])